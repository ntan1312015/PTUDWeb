(function() {
    'use strict';

    angular
        .module('cv')
        .controller('MainController', MainController);

    /** @ngInject */
    function MainController($timeout, $scope) {
        var vm = this;
        vm.form = true;

        vm.technical = [{
            name: 'C/C++',
            value: '80%'
        }, {
            name: 'C#',
            value: '85%'
        }, {
            name: 'Html',
            value: '65%'
        }, {
            name: 'Java',
            value: '50%'
        }, {
            name: 'Android',
            value: '70%'
        }];

        vm.soft = [{
            name: 'Communication',
            value: '60%'
        }, {
            name: 'Team work',
            value: '75%'
        }, {
            name: 'Creativity',
            value: '50%'
        }, {
            name: 'Dedication',
            value: '70%'
        }, {
            name: 'volunteerism',
            value: '65%'
        }];

        vm.information = [{
            name: 'Full name:',
            value: 'Nguyen Thanh An'
        }, {
            name: 'Nationality:',
            value: 'Vietnamese'
        }, {
            name: 'Place of birth:',
            value: 'Tien Giang Province'
        }, {
            name: 'Date of birth:',
            value: '06/23/1995'
        }, {
            name: 'Gender:',
            value: 'Male'
        }, {
            name: 'Marital status:',
            value: 'Single'
        }, {
            name: 'Email:',
            value: 'ntan1312015@gmail.com'
        }, {
            name: 'Languages:',
            value: 'Vietnamese(Native), English(Normal)'
        }];

        vm.hobbies = [{
            name: 'Traveling'
        }, {
            name: 'Electronic Music, Instrumental Music'
        }, {
            name: 'Cartoon Film, Action Film'
        }, {
            name: 'Motorcycle'
        }, {
            name: 'Riding'
        }, {
            name: 'Reading Motorcycle Information'
        }];

        vm.user = {};
        vm.user.fullname = '';
        vm.user.email = '';
        vm.user.phonenumber = '';
        vm.user.subject = '';
        vm.user.message = '';

        $scope.$watchGroup(['vm.user.fullname', 'vm.user.email', 'vm.user.phonenumber', 'vm.user.subject', 'vm.user.message '], function(n, o, scope) {
            if (n[0] !== '' && n[1] !== '' && n[2] !== '' && n[3] !== '' && n[4] !== '') {
                vm.form = false;
            }
        });
    }
})();